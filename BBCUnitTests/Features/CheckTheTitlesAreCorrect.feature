﻿Feature: CheckTheMainNewsTitleIsCorrect
	In order to be sure
	That news are displayed in correspondency to expectations
	I want to be shown the titles of news according to their priority

@mytag
Scenario: Check the title of main news
	Given the main page of the website is opened and I navigate to News	
	Then I can see and compare the title of main news "Europe's weekly coronavirus cases pass March peak"

Scenario: Check The title of secondary news
    Given the main page of the website is opened and I navigate to News			
	Then I can see and compare the titles of secondary news with expected ones
	| Title                                                    |
	| Floods and destruction as storm Sally moves north        |
	| Navalny's aides say poison found on water bottle         |
	| Why fires in Siberia threaten us all                     |
	| Netflix cheer star 'produced child sex images'           |
	| Australia ex-PM  hacked after Instagramming boarding pass|

Scenario: Check The title contains keyword
    Given the main page of the website is opened and I navigate to News		
	When I store and search the text from location tag of main news title	
    Then I can see the first title of articles matches keyword "Europe"

