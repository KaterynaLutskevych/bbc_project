﻿Feature: FillTheForm
	In order to share the story with BBC
	When I navigate to the page with special form
	I can fill in personal data and submit information only if all required fields are filled with valid data

@negative_scenarios
Scenario: Ask the question to BBC without checking Age checkbox
	Given I navigate to News	
	When I navigate to the page with special form
	And I fill the required fields with valid data except Age checkbox
	Then I can see the error for Age checkbox is not checked "must be accepted"

Scenario: Ask the question to BBC without filling Email field
	Given I navigate to News
	When I navigate to the page with special form
	And I fill the required fields with valid data except Email
	Then I can see the error for Email field is not filled "blank"

Scenario: Ask the question to BBC without filling Question field
	Given I navigate to News	
	When I navigate to the page with special form
	And I fill the required fields with valid data except Question
	Then I can see the error for Question field is not filled "blank"

Scenario: Ask the question to BBC without filling Name field
	Given I navigate to News
	When I navigate to the page with special form
	And I fill the required fields with valid data except Name
	Then I can see the error for name field is not filled "blank"
