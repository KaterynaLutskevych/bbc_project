﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;


namespace BBCUnitTests
{
    public class HaveYourSayPage : BasePage
    {        
        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'5214')]")] 
        private IWebElement shareWithBbc;
        
        public HaveYourSayPage() : base() { }

        public void ClickShareWithBbc()
        {
            shareWithBbc.Click();
        }
    }
}
