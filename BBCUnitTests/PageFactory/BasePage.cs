using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;


namespace BBCUnitTests
{
    public class BasePage
    {       
        public BasePage()
        {           
            PageFactory.InitElements(ClassForDriver.Driver, this);
        }
        public void ImplicitWait()
        {
            ClassForDriver.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        }
        public bool IsElementVisible(IWebElement element)
        {
            return element.Displayed && element.Enabled;
        }
        public void WaitForPageReadyState(long timeToWait)
        {
            new WebDriverWait(ClassForDriver.Driver, TimeSpan.FromSeconds(timeToWait)).Until(
                    webDriver=> ((IJavaScriptExecutor)ClassForDriver.Driver).ExecuteScript("return document.readyState").Equals("complete"));
        }       
    }
}
