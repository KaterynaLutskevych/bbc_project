﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;


namespace BBCUnitTests
{
    public class MainPage : BasePage
    {       
        [FindsBy(How = How.XPath, Using = "//nav[@role]//a[contains(text(),'News')]")]
        private IWebElement newsTitle;
        
        public MainPage() : base() {}

        public void ClickOnNewsTitle()
        {
            newsTitle.Click();
        }
    }
}
