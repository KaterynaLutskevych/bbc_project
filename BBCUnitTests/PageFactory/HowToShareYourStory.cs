﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;


namespace BBCUnitTests
{
    public class HowToShareYourStory : BasePage
    {       
        [FindsBy(How = How.XPath, Using = "//textarea[@placeholder='Let us know your question.']")] 
        private IWebElement tellStoryField;

        [FindsBy(How = How.XPath, Using = "//input[@aria-label='Name']")] 
        private IWebElement nameField;

        [FindsBy(How = How.XPath, Using = "//input[@aria-label='Email address']")]
        private IWebElement emailField;

        [FindsBy(How = How.XPath, Using = "//input[@aria-label='Telephone number']")]
        private IWebElement phoneField;

        [FindsBy(How = How.XPath, Using = "(//input[@type='checkbox'])[1]")]
        private IWebElement ageCheckbox;
       
        [FindsBy(How = How.XPath, Using = "(//input[@type='checkbox'])[2]")]
        private IWebElement acceptTermsCheckbox;

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Submit')]")]
        private IWebElement submitButton;

        [FindsBy(How = How.XPath, Using = "(//div[contains(text(),'blank')])[1]")]
        private IWebElement questionError;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Name')]")]
        private IWebElement nameError;

        [FindsBy(How = How.XPath, Using = "//div[contains(text(),'Email')]")]
        private IWebElement emailError;

        [FindsBy(How = How.XPath, Using = "(//div[contains(text(),'must be')])[1]")]
        private IWebElement ageCheckboxError;

        [FindsBy(How = How.XPath, Using = "(//div[contains(text(),'must be')])[2]")]
        private IWebElement termsCheckboxError;
        
        public HowToShareYourStory() : base() { }

        public IWebElement GetTellStoryField()
        {
            return tellStoryField;
        }
        public IWebElement GetNameField()
        {
            return nameField;
        }
        public IWebElement GetEmailField()
        {
            return emailField;
        }
        public IWebElement GetPhoneField()
        {
            return phoneField;
        }
        public void ClickAgeCheckbox()
        {
            ageCheckbox.Click();
        }
        public void ClickAcceptTermsCheckbox()
        {
            acceptTermsCheckbox.Click();
        }
        public void ClickSubmitButton()
        {
            submitButton.Click();
        }
        public void SendKeysToQuestionField()
        {
            tellStoryField.SendKeys("My question");
        }
        public void SendKeysToNameField()
        {
            nameField.SendKeys("Kate");
        }
        public void SendKeysToEmailField()
        {
            emailField.SendKeys("Kate@ukr.net");
        }
        public void SendKeysToPhoneField()
        {
            phoneField.SendKeys("0445304080");
        }
        public IWebElement GetQuestionError()
        {
            return questionError;
        }
        public IWebElement GetNameError()
        {
            return nameError;
        }
        public IWebElement GetEmailError()
        {
            return emailError;
        }
        public IWebElement GetAgeCheckboxError()
        {
            return ageCheckboxError;
        }
        public IWebElement GetTermsCheckboxError()
        {
            return termsCheckboxError;
        }

    }
}
