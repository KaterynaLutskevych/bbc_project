﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;


namespace BBCUnitTests
{
    public class BBCNewsPage : BasePage
    {       
        public BBCNewsPage() : base()  {}

        [FindsBy(How = How.XPath, Using = "//button[@class='sign_in-exit']")]
        private IWebElement closePopup;

        [FindsBy(How = How.XPath, Using = "(//div[contains(@data-entityid,'stories#1')]//a[contains(@class,'promo-heading')])[1]")]
        private IWebElement titleOfPrimaryNews;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'secondary')]//a[contains(@class,'heading')]")]
        private IList<IWebElement> elementList;

        [FindsBy(How = How.XPath, Using = "(//div[contains(@data-entityid,'stories#1')]//a[contains(@class,'section-link')])[1]")]
        private IWebElement storedText;

        [FindsBy(How = How.XPath, Using = "//input[@id='orb-search-q']")]
        private IWebElement searchField;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'navigation')]//a[contains(@href,'coronavirus')]")]
        private IWebElement coronavirusCategory;

        
        public void ClickClosePopup()
        {
            closePopup.Click();
        }
        public String GetTitleOfPrimaryNews()
        {
            return titleOfPrimaryNews.Text;
        }
        public IList<IWebElement> GetElementList()
        {
            return elementList;
        }
        public String GetStoredtext()
        {
            return storedText.Text;
        }
        public IWebElement GetSearchField()
        {
            return searchField;
        }
        public void ClickCoronavirusCategory()
        {
            coronavirusCategory.Click();
        }
    }
}
