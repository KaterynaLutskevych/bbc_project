﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;


namespace BBCUnitTests
{
    public class CoronavirusPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'secondary')]//a[contains(@href,'have_your_say')]")]
        private IWebElement yourCoronavirusStories;
        
        public CoronavirusPage() : base() { }

        public void ClickYourCoronavirusStories()
        {
            yourCoronavirusStories.Click();
        }
        
    }
}
