﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;


namespace BBCUnitTests
{
    public class SearchResultPage : BasePage
    {       
        [FindsBy(How = How.XPath, Using = "(//a[contains(@class,'PromoLink')])[1]")]
        private IWebElement firstTitleInCategory;
        
        public SearchResultPage() : base() {}
        
        public String GetFirstTitleInCategory()
        {
            return firstTitleInCategory.Text;
        }


    }
}
