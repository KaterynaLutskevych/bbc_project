﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace BBCUnitTests
{
    public static class ClassForDriver
    {
        private static IWebDriver driver;
        public static IWebDriver Driver
        {
            get
            {
                if (driver == null)
                {
                    driver = new ChromeDriver();
                }
                return driver;
            }
        }
    }
}
