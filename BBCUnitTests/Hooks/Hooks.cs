using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace BBCUnitTests.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        
        [BeforeScenario]
        public void BeforeScenario()
        {   
            new BasePage().ImplicitWait();         
            ClassForDriver.Driver.Manage().Window.Maximize();
            ClassForDriver.Driver.Navigate().GoToUrl("https://www.bbc.com");
        }

        [AfterScenario]
        public void AfterScenario()
        {
            ClassForDriver.Driver.Quit();
        }
    }
}
