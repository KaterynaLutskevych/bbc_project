using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace BBCUnitTests.StepDefinitions
{
    [Binding]
    public class FillTheFormSteps 
    {
        HowToShareYourStory howToShareYourStory = new HowToShareYourStory();       
        BBCNewsPage bBCNewsPage = new BBCNewsPage();

        [Given(@"I navigate to News")]
        public void GivenINavigateToNews()
        {
            new MainPage().ClickOnNewsTitle();
            bBCNewsPage.ClickClosePopup();
        }
                
        [When(@"I navigate to the page with special form")]
        public void WhenINavigateToThePageWithSpecialForm()
        {
            bBCNewsPage.ClickCoronavirusCategory();
            new CoronavirusPage().ClickYourCoronavirusStories();
            new HaveYourSayPage().ClickShareWithBbc();      
        }

        [When(@"I fill the required fields with valid data except Age checkbox")]
        public void WhenIFillTheRequiredFieldsWithValidDataExceptAgeCheckbox()
        {
            howToShareYourStory.SendKeysToQuestionField();
            howToShareYourStory.SendKeysToNameField();
            howToShareYourStory.SendKeysToEmailField();
            howToShareYourStory.SendKeysToPhoneField();
            howToShareYourStory.ClickAcceptTermsCheckbox();
            howToShareYourStory.ClickSubmitButton();           
        }

        [When(@"I fill the required fields with valid data except Email")]
        public void WhenIFillTheRequiredFieldsWithValidDataExceptEmail()
        {
            howToShareYourStory.SendKeysToQuestionField();
            howToShareYourStory.SendKeysToNameField();
            howToShareYourStory.SendKeysToPhoneField();
            howToShareYourStory.ClickAgeCheckbox();
            howToShareYourStory.ClickAcceptTermsCheckbox();
            howToShareYourStory.ClickSubmitButton();           
        }

        [When(@"I fill the required fields with valid data except Question")]
        public void WhenIFillTheRequiredFieldsWithValidDataExceptQuestion()
        {
            howToShareYourStory.SendKeysToNameField();
            howToShareYourStory.SendKeysToEmailField();
            howToShareYourStory.SendKeysToPhoneField();
            howToShareYourStory.ClickAgeCheckbox();
            howToShareYourStory.ClickAcceptTermsCheckbox();
            howToShareYourStory.ClickSubmitButton();           
        }

        [When(@"I fill the required fields with valid data except Name")]
        public void WhenIFillTheRequiredFieldsWithValidDataExceptName()
        {
            howToShareYourStory.SendKeysToQuestionField();
            howToShareYourStory.SendKeysToEmailField();
            howToShareYourStory.SendKeysToPhoneField();
            howToShareYourStory.ClickAgeCheckbox();
            howToShareYourStory.ClickAcceptTermsCheckbox();
            howToShareYourStory.ClickSubmitButton();           
        }
      
        [Then(@"I can see the error for Age checkbox is not checked ""(.*)""")]
        public void ThenICanSeeTheErrorForAgeCheckboxIsNotChecked(string message)
        {                                                                            
            Assert.IsTrue(howToShareYourStory.GetAgeCheckboxError().Text.Contains(message));             
        }
        [Then(@"I can see the error for Email field is not filled ""(.*)""")]
        public void ThenICanSeeTheErrorForEmailFieldIsNotFilled(string message)
        {
            Assert.IsTrue(howToShareYourStory.GetEmailError().Text.Contains(message));
        }
        [Then(@"I can see the error for Question field is not filled ""(.*)""")]
        public void ThenICanSeeTheErrorForQuestionFieldIsNotFilled(string message)
        {
            Assert.IsTrue(howToShareYourStory.GetQuestionError().Text.Contains(message));
        }
        [Then(@"I can see the error for name field is not filled ""(.*)""")]
        public void ThenICanSeeTheErrorForNameFieldIsNotFilled(string message)
        {
            Assert.IsTrue(howToShareYourStory.GetNameError().Text.Contains(message));
        }       
    }
}
