using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;


namespace BBCUnitTests.StepDefinitions
{
    [Binding]
    public class CheckTheTitlesAreCorrectSteps 
    {
        BBCNewsPage bbcNewsPage = new BBCNewsPage();

        [Given(@"the main page of the website is opened and I navigate to News")]
        public void GivenTheMainPageOfTheWebsiteIsOpened()
        {
            new MainPage().ClickOnNewsTitle();           
            bbcNewsPage.ClickClosePopup();            
        }

        [Then(@"I can see and compare the title of main news ""(.*)""")]
        public void ThenICanSeeAndCompareTheTitleOfMainNews(string title)
        {           
            Assert.AreEqual(title, bbcNewsPage.GetTitleOfPrimaryNews());
        }

        [Then(@"I can see and compare the titles of secondary news with expected ones")]
        public void ThenICanSeeAndCompareTheTitlesOfSecondaryNewsWithExpectedOnes(Table titles)
        {
            List<String> list1 = new List<String>();
            foreach (var title in titles.Rows)
            {
                list1.Add(title[0]);
            }
            List<String> list2 = new List<String>();            
            foreach (IWebElement el in bbcNewsPage.GetElementList())
            {
                list2.Add(el.Text);
            }
            CollectionAssert.AreEquivalent(list1, list2);
        }

        [When(@"I store and search the text from location tag of main news title")]
        public void WhenIStoreAndSearchTheTextFromLocationTagOfMainNewsTitle()
        {
            bbcNewsPage.GetSearchField().SendKeys(bbcNewsPage.GetStoredtext() + Keys.Enter);          
        }

        [Then(@"I can see the first title of articles matches keyword ""(.*)""")]
        public void ThenICanSeeTheFirstTitleOfArticlesMatchesKeyword(string keyword)
        {                     
            Assert.IsTrue(new SearchResultPage().GetFirstTitleInCategory().Contains(keyword));            
        }
    }
}
